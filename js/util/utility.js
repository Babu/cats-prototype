$(document).ready(function() {

  

     $('#crewsummarytable').DataTable( {
        searching:false,        
        paging: false,
        info:false,
        ordering:false,
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0,
            sTitle: "<span>SR</span>"
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    } ); 
   
     $('#crewdetailsTable').DataTable( {
        searching:false,        
        paging: false,
        info:false,
        ordering:false
     }); 
   
     $('#crewpayperiodtable1').DataTable( {
        searching:false,        
        paging: false,
        info:false,
        ordering:false,
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0,
            sTitle: "<span>SR</span>"
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
     });
 
      $("#file_menu").on('click',function(event){
        
           $(".showTabsMenu").hide();
         $(".showFileMenu").toggle();
           event.stopPropagation();
      });

      $("#tabs_menu").on('click',function(event){
           
        $(".showFileMenu").hide();
         $(".showTabsMenu").toggle();
           event.stopPropagation();
      });
        $(".showFileMenu ").on('click', function(event){
            event.stopPropagation();
        });
        $(".showTabsMenu").on('click', function(event){
            event.stopPropagation();
        });
    $(window).click(function(e) {
    //Hide the menus if visible
     $(".showFileMenu").hide();
      $(".showTabsMenu").hide();
    });

  
    $('[data-toggle="tooltip"]').tooltip();
    $('.showtoolTip').tooltip();

    $('#datepicker').datepicker({
        format: "dd/mm/yyyy"
    }); 


} );
